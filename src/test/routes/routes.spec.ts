import chai, { expect } from "chai";
import chaiHttp from "chai-http";
import server from "../../main/index";
import mongoUnit from "mongo-unit";
import axios from "axios";
import MockAdapter from "axios-mock-adapter";
import configWorld from "../../main/config/world";
import configAuth from "../../main/config/auth";
import { ApiConfiguration } from "../../main/config/configuration";

chai.use(chaiHttp);
const mock = new MockAdapter(axios);

describe("Routes", () => {

    beforeEach(() => {

        mock.onGet(`${(configAuth.config as ApiConfiguration).host}:${(configAuth.config as ApiConfiguration).port}/auth/verify`).reply(200, {});

        const db = `
            {
                "Game": [
                    {
                        "_id": "56d9bf92f9be48771d6fe5b1",
                        "username": "testUser",
                        "gamename": "testGame",
                        "creationDate": "2012-11-04T14:51:06.157Z"
                    },
                    {
                        "_id": "56d9bf92f9be48771d6fe5b2",
                        "username": "testUser",
                        "gamename": "testGame2",
                        "creationDate": "2012-11-04T14:51:06.157Z"
                    },
                    {
                        "_id": "56d9bf92f9be48771d6fe5b3",
                        "username": "testUser",
                        "gamename": "testGame3",
                        "creationDate": "2012-11-04T14:51:06.157Z"
                    },
                    {
                        "_id": "56d9bf92f9be48771d6fe5b4",
                        "username": "testUser",
                        "gamename": "testGame4",
                        "creationDate": "2012-11-04T14:51:06.157Z"
                    },
                    {
                        "_id": "56d9bf92f9be48771d6fe5b5",
                        "username": "testUser",
                        "gamename": "testGame5",
                        "creationDate": "2012-11-04T14:51:06.157Z"
                    },
                    {
                        "_id": "56d9bf92f9be48771d6fe5b6",
                        "username": "testUser",
                        "gamename": "testGame6",
                        "creationDate": "2012-11-04T14:51:06.157Z"
                    }
                ]
            }`;


        mongoUnit.initDb(JSON.parse(db));
    });
    afterEach(() => mongoUnit.drop());
   
    describe("/GET games", () => {
        it("It should return six games from testUser", (done) => {
            chai.request(server).get("/api/v1/testUser/games").end((err, res) => {
                expect(res.status).to.equal(200);
                expect(res.body.data.length).to.equal(6);
                expect(res.body.error).to.equal(null);
                done();
            });
        });

        it("It should return second game from testUser with full search", (done) => {
            chai.request(server).get("/api/v1/testUser/games?search=testGame2&").end((err, res) => {
                expect(res.status).to.equal(200);
                expect(res.body.data.length).to.equal(1);
                expect(res.body.data[0].gamename).to.equal("testGame2");
                expect(res.body.error).to.equal(null);
                done();
            });
        });

        it("It should return second game from testUser whith short search", (done) => {
            chai.request(server).get("/api/v1/testUser/games?search=Game2&").end((err, res) => {
                expect(res.status).to.equal(200);
                expect(res.body.data.length).to.equal(1);
                expect(res.body.data[0].gamename).to.equal("testGame2");
                expect(res.body.error).to.equal(null);
                done();
            });
        });

        it("It should return second game from testUser with case search", (done) => {
            chai.request(server).get("/api/v1/testUser/games?search=gAmE2&").end((err, res) => {
                expect(res.status).to.equal(200);
                expect(res.body.data.length).to.equal(1);
                expect(res.body.data[0].gamename).to.equal("testGame2");
                expect(res.body.error).to.equal(null);
                done();
            });
        });

        it("It should return first game from testUser", (done) => {
            chai.request(server).get("/api/v1/testUser/games?start=0&end=1&").end((err, res) => {
                expect(res.status).to.equal(200);
                expect(res.body.data.length).to.equal(1);
                expect(res.body.data[0].gamename).to.equal("testGame");
                expect(res.body.error).to.equal(null);
                done();
            });
        });

        it("It should return second game from testUser", (done) => {
            chai.request(server).get("/api/v1/testUser/games?start=1&end=1&").end((err, res) => {
                expect(res.status).to.equal(200);
                expect(res.body.data.length).to.equal(1);
                expect(res.body.data[0].gamename).to.equal("testGame2");
                expect(res.body.error).to.equal(null);
                done();
            });
        });

        it("It should return two games from testUser", (done) => {
            chai.request(server).get("/api/v1/testUser/games?start=0&end=2&").end((err, res) => {
                expect(res.status).to.equal(200);
                expect(res.body.data.length).to.equal(2);
                expect(res.body.error).to.equal(null);
                done();
            });
        });

        it("It should return third game from testUser", (done) => {
            chai.request(server).get("/api/v1/testUser/games?start=2&end=3&").end((err, res) => {
                expect(res.status).to.equal(200);
                expect(res.body.data.length).to.equal(1);
                expect(res.body.data[0].gamename).to.equal("testGame3");
                expect(res.body.error).to.equal(null);
                done();
            });
        });

        it("It should return third game from testUser", (done) => {
            chai.request(server).get("/api/v1/testUser/games?start=2&end=4&").end((err, res) => {
                expect(res.status).to.equal(200);
                expect(res.body.data.length).to.equal(2);
                expect(res.body.data[0].gamename).to.equal("testGame3");
                expect(res.body.data[1].gamename).to.equal("testGame4");
                expect(res.body.error).to.equal(null);
                done();
            });
        });

        it("It should return zero game from user", (done) => {
            chai.request(server).get("/api/v1/user/games").end((err, res) => {
                expect(res.status).to.equal(200);
                expect(res.body.data.length).to.equal(0);
                expect(res.body.error).to.equal(null);
                done();
            });
        });

        it("It should return zero game from unknown user", (done) => {
            chai.request(server).get("/api/v1/hey/games").end((err, res) => {
                expect(res.status).to.equal(200);
                expect(res.body.data.length).to.equal(0);
                expect(res.body.error).to.equal(null);
                done();
            });
        });
    });

    describe("/PUT games", () => {
        it("It should create new game for user", (done) => {

            const url = new RegExp(`${(configWorld.config as ApiConfiguration).host}:${(configWorld.config as ApiConfiguration).port}/games/.*`);

            mock.onPost(url).reply(200, {});

            chai.request(server).post("/api/v1/user/games").send({ gamename: "gamenameTest" }).end((err, res) => {
                expect(res.status).to.equal(200);
                expect(res.body.error).to.equal(null);

                const id = String(res.body.data);   

                describe("/GET game", () => {
                    chai.request(server).get("/api/v1/user/games/" + id).end((err, res) => {
                        expect(res.status).to.equal(200);
                        expect(res.body.data.username).to.equal("user");
                        expect(res.body.data.gamename).to.equal("gamenameTest");
                        expect(res.body.error).to.equal(null);
                        done();
                    });
                });
            });
        });

        it("It should raise an error with 500", (done) => {

            const url = new RegExp(`${(configWorld.config as ApiConfiguration).host}:${(configWorld.config as ApiConfiguration).port}/games/.*`);

            mock.onPost(url).timeout();

            chai.request(server).post("/api/v1/user/games").send({ gamename: "gamenameTest" }).end((err, res) => {
                expect(res.status).to.equal(500);
                expect(res.body.error).to.equal("INVALID_API_WORLD");
                done();
            });
        });

        it("It should create new game for user with integer as name", (done) => {

            const url = new RegExp(`${(configWorld.config as ApiConfiguration).host}:${(configWorld.config as ApiConfiguration).port}/games/.*`);

            mock.onPost(url).reply(200, {});

            chai.request(server).post("/api/v1/user/games").send({ gamename: 50 }).end((err, res) => {
                expect(res.status).to.equal(200);
                expect(res.body.error).to.equal(null);

                const id = String(res.body.data);   

                describe("/GET game", () => {
                    chai.request(server).get("/api/v1/user/games/" + id).end((err, res) => {
                        expect(res.status).to.equal(200);
                        expect(res.body.data.username).to.equal("user");
                        expect(res.body.data.gamename).to.equal("50");
                        expect(res.body.error).to.equal(null);
                        done();
                    });
                });
            });
        });
    });

    describe("/DELETE game", () => {
        it("It should delete game for user", (done) => {

            mock.onDelete(`${(configWorld.config as ApiConfiguration).host}:${(configWorld.config as ApiConfiguration).port}/games/56d9bf92f9be48771d6fe5b2`).reply(200, {});

            chai.request(server).delete("/api/v1/testUser/games/56d9bf92f9be48771d6fe5b2").end((err, res) => {
                expect(res.status).to.equal(200);
                expect(res.body.error).to.equal(null);

                describe("/GET game", () => {
                    chai.request(server).get("/api/v1/testUser/games/56d9bf92f9be48771d6fe5b2").end((err, res) => {
                        expect(res.status).to.equal(200);
                        expect(res.body.data).to.equal(null);
                        expect(res.body.error).to.equal(null);
                        done();
                    });
                });
            });
        });

        it("It should raise an error with 400", (done) => {

            mock.onDelete(`${(configWorld.config as ApiConfiguration).host}:${(configWorld.config as ApiConfiguration).port}/games/key`).reply(200, {});

            chai.request(server).delete("/api/v1/testUser/games/key").end((err, res) => {
                expect(res.status).to.equal(400);
                expect(res.body.error).to.equal("INVALID_ID");
                done();
            });
        });

        it("It should raise an error with 500", (done) => {

            mock.onDelete(`${(configWorld.config as ApiConfiguration).host}:${(configWorld.config as ApiConfiguration).port}/games/56d9bf92f9be48771d6fe5b9`).timeout();

            chai.request(server).delete("/api/v1/testUser/games/56d9bf92f9be48771d6fe5b9").end((err, res) => {
                expect(res.status).to.equal(500);
                expect(res.body.error).to.equal("INVALID_API_WORLD");
                done();
            });
        });

        it("It should return nothing", (done) => {

            mock.onDelete(`${(configWorld.config as ApiConfiguration).host}:${(configWorld.config as ApiConfiguration).port}/games/56d9bf92f9be48771d6fe5b8`).reply(200, {});

            chai.request(server).delete("/api/v1/testUser/games/56d9bf92f9be48771d6fe5b8").end((err, res) => {
                expect(res.status).to.equal(200);
                expect(res.body.error).to.equal(null);
                done();
            });
        });
    });

    describe("/GET game", () => {
        it("It should raise an error", (done) => {
            chai.request(server).delete("/api/v1/testUser/games/key").end((err, res) => {
                expect(res.status).to.equal(400);
                expect(res.body.error).to.equal("INVALID_ID");
                done();
            });
        });
    });

    describe("/PATCH game", () => {
        it("It should update gamename", (done) => {
            chai.request(server).patch("/api/v1/testUser/games/56d9bf92f9be48771d6fe5b6").send({ gamename: "gamenameTest" }).end((err, res) => {
                expect(res.status).to.equal(200);
                expect(res.body.data).to.equal("56d9bf92f9be48771d6fe5b6");
                done();
            });
        });

        it("It should raise an error", (done) => {
            chai.request(server).patch("/api/v1/testUser/games/hey").send({ gamename: "gamenameTest" }).end((err, res) => {
                expect(res.status).to.equal(400);
                expect(res.body.error).to.equal("INVALID_ID");
                done();
            });
        });
    });
});