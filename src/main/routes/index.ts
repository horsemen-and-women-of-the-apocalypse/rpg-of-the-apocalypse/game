/**
 * @licence
 * Copyright 2022-2022 - RPG of the Apocalypse
 * Licensed under MIT or any later version
 * Refer to the LICENSE file included.
 */
import { Router } from "express";
import version from "./version";
import game from "./game";

/**
 * API route
 */
class Route {
    /** Base path for all routes defined by {@link router} */
    public readonly base: string;

    /** Router containing routes to declare */
    public readonly router: Router;

    /**
     * Constructor
     *
     * @param base Base path
     * @param router Router
     */
    constructor(base: string, router: Router) {
        this.base = base;
        this.router = router;
    }
}

export default [ new Route("/version", version), new Route("/api/v1", game) ];
