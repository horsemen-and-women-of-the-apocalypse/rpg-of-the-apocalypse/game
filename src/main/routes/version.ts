/**
 * @licence
 * Copyright 2022-2022 - RPG of the Apocalypse
 * Licensed under MIT or any later version
 * Refer to the LICENSE file included.
 */
import { Router } from "express";
import { ApiResponse } from "@rpga/common";

const router: Router = Router();

router.get("/", (req, res) => {
    res.send(new ApiResponse(process.env.npm_package_version));
});

export default router;
