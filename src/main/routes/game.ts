/**
 * @licence
 * Copyright 2022-2022 - RPG of the Apocalypse
 * Licensed under MIT or any later version
 * Refer to the LICENSE file included.
 */
import { Router } from "express";
import { Database } from "../db/database";
import { ApiError, ApiErrorResponse, ApiResponse } from "@rpga/common";
import { ObjectId } from "mongodb";
import axios from "axios";
import LOGGER from "../logger";
import config from "../config/world";
import { ApiConfiguration } from "../config/configuration";

const router : Router = Router();

const db: Database = new Database();

/**
 * Get all games from user
 */
router.get("/:username/games", (req, resp) => {
    const username = String(req.params.username);

    const queryParams = req.query;

    const search = queryParams["search"] === undefined ? "" : String(queryParams["search"]);
    const start = isNaN(Number.parseInt(String(queryParams["start"]))) ? 0 : Number.parseInt(String(queryParams["start"]));
    const end = isNaN(Number.parseInt(String(queryParams["end"]))) ? Number.MAX_SAFE_INTEGER : Number.parseInt(String(queryParams["end"]));

    if(start < 0 || end < 0 || !Number.isFinite(start) || !Number.isFinite(end) || end < start) {
        resp.status(400).json(new ApiErrorResponse("INVALID_NUMBER"));
    }

    db.getGames(username, search, start, end).then((games) => {
        resp.send(new ApiResponse(games));
    }).catch((e) => {
        resp.status(e instanceof ApiError ? (e as ApiError).code : 500).json(new ApiErrorResponse("INVALID_BDD_REQUEST : " + e.message));
    });
});

/*
 * Post a new game for user
 */
router.post("/:username/games", (req, resp) => {
    const username = String(req.params.username);
    const gamename = String(req.body.gamename);

    const token = String(req.headers.token);

    db.postGame(username, gamename).then((id) => {

        LOGGER.info(`[Axios] Requête vers : ${(config.config as ApiConfiguration).port}:${(config.config as ApiConfiguration).port}$/games/${id}`);

        axios.post(`${(config.config as ApiConfiguration).host}:${(config.config as ApiConfiguration).port}/games/${id}`, {
            headers: {
                "authorization": token
            },
        }, {
            timeout: 2000
        }).then(() => { 
            resp.send(new ApiResponse(id));
        }).catch((error) => {
            db.deleteGame(id).then( () => {
                if(error.response) {
                    resp.status(error.response.status).json(new ApiErrorResponse(error.response.data.error));
                } else {
                    resp.status(500).json(new ApiErrorResponse("INVALID_API_WORLD"));
                }     
            });    
        }); 
    }).catch((e) => {
        resp.status(500).json(new ApiErrorResponse("INVALID_BDD_REQUEST : " + e.message));
    });
});

/**
 * Get specific game from user
 */
router.get("/:username/games/:id", (req, resp) => {
    
    if(!ObjectId.isValid(req.params.id)) {
        resp.status(400).json(new ApiErrorResponse("INVALID_ID"));
    }
    
    const id = new ObjectId(req.params.id);

    db.getGame(id).then((game) => {
        resp.send(new ApiResponse(game));
    }).catch((e) => {
        resp.status(500).json(new ApiErrorResponse("INVALID_BDD_REQUEST : " + e.message));
    });
});

/**
 * Delete specific game from user
 */
router.delete("/:username/games/:id", (req, resp) => {

    if(!ObjectId.isValid(req.params.id)) {
        resp.status(400).json(new ApiErrorResponse("INVALID_ID"));
    }

    const id = new ObjectId(req.params.id);

    const token = String(req.headers.token);

    LOGGER.info(`[Axios] Requête vers : ${(config.config as ApiConfiguration).host}:${(config.config as ApiConfiguration).port}/games//${id}`);

    axios.delete(`${(config.config as ApiConfiguration).host}:${(config.config as ApiConfiguration).port}/games/${id}`, {
        headers: {
            "authorization": token
        },
        timeout: 2000
    }).then(() => { 
        db.deleteGame(id).then((id) => {
            resp.send(new ApiResponse(id));
        }).catch((e) => {
            resp.status(500).json(new ApiErrorResponse("INVALID_BDD_REQUEST : " + e.message));
        });
    }).catch((error) => {
        if(error.response) {
            resp.status(error.response.status).json(new ApiErrorResponse(error.response.data.error));
        } else {
            resp.status(500).json(new ApiErrorResponse("INVALID_API_WORLD"));
        }
    });
});

/**
 * Patch specific game from user
 */
router.patch("/:username/games/:id", (req, resp) => {
    
    if(!ObjectId.isValid(req.params.id)) {
        resp.status(400).json(new ApiErrorResponse("INVALID_ID"));
    }
    
    const id = new ObjectId(req.params.id);
    const gamename = String(req.body.gamename);
    
    db.patchGame(id, gamename).then((id) => {
        resp.send(new ApiResponse(id));
    }).catch((e) => {
        resp.status(500).json(new ApiErrorResponse("INVALID_BDD_REQUEST : " + e.message));
    });
});

export default router;
