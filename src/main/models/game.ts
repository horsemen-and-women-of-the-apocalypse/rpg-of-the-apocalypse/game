/**
 * @licence
 * Copyright 2022-2022 - RPG of the Apocalypse
 * Licensed under MIT or any later version
 * Refer to the LICENSE file included.
 */
import { ObjectId } from "mongodb";

/**
 * Class representing Game database object
 */
export default class Game {

    _id: ObjectId;
    username: string;
    gamename: string;
    creationDate: Date;

    /**
     * Private constructor creating a game if id is empty and copying if not
     * 
     * @param id
     * @param username 
     * @param gamename
     * @param creationDate 
     */
    private constructor(id: ObjectId | undefined, username: string, gamename: string, creationDate: Date |undefined) {
        this._id = id === undefined ? new ObjectId() : id;
        this.username = username;
        this.gamename = gamename;
        this.creationDate = creationDate === undefined ? new Date() : creationDate;
    }

    /**
     * Create a new game
     * 
     * @param username 
     * @param gamename 
     */
    public static create(username: string, gamename: string) {
        return new Game(undefined, username, gamename, undefined);
    }

    /**
     * Generate an existing game
     * 
     * @param id 
     * @param username 
     * @param gamename
     * @param creationDate
     */
    public static generate(id: ObjectId, username: string, gamename: string, creationDate: Date) {
        return new Game(id, username, gamename, creationDate);
    }
}