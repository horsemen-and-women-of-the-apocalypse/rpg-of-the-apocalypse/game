/**
 * @licence
 * Copyright 2022-2022 - RPG of the Apocalypse
 * Licensed under MIT or any later version
 * Refer to the LICENSE file included.
 */
export default class Validator {

    public static validate(...args: Array<unknown>) {
        args.forEach(arg => {
            if(!arg) {
                throw Error("Argument is null or undefined");
            }
        });
    }
}
