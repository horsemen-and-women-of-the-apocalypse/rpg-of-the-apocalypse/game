/**
 * @licence
 * Copyright 2022-2022 - RPG of the Apocalypse
 * Licensed under MIT or any later version
 * Refer to the LICENSE file included.
 */
import { MongoClient, Db , Collection, ObjectId } from "mongodb";
import config from "../config/database";
import Game from "../models/game";
import Validator from "../tools/validator";
import LOGGER from "../logger/index";
import { DatabaseConfiguration } from "../config/configuration";

/**
 * Class to manage database
 * - Connection
 * - GET/POST/DELETE/PATCH
 */
class Database {

    client: MongoClient;
    collections: { games?: Collection };

    /**
     * Init connection to database
     * 
     */
    constructor() {
        this.client = new MongoClient((config.config as DatabaseConfiguration).url);
        this.collections = {};

        this.connect();
    }

    /**
     * Connect to database
     * 
     */
    private async connect() {

        LOGGER.info("[Database] Connecting to database");
    
        this.client.connect().then(() => {
            LOGGER.info("[Database] Successfully connected to server");
        }).catch((e) => {
            LOGGER.error("[Database] Failed to connect to server " + e);
        });
    
        const db: Db = this.client.db((config.config as DatabaseConfiguration).name);

        const gamesCollection: Collection = db.collection("Game");
        this.collections.games = gamesCollection;
    }

    /**
     * Get all games from username
     * 
     * @param username 
     */
    public async getGames(username: string, search: string, start: number , end: number ) {

        Validator.validate(this.collections, this.collections.games, username);

        /* eslint-disable  @typescript-eslint/no-non-null-assertion */
        const result = (await this.collections!.games!.find({ "username": username, "gamename": { $regex: `^.*${search}.*$`, $options: "i" } }).skip(start > 0 ? start : 0).limit(end - ((start != end) ? start : 0)).toArray());

        const games: Game[] = [];

        if(!result) {
            return null;
        }

        result.forEach((game) => games.push(Game.generate(game._id, game.username, game.gamename, game.creationDate)));

        return games;

    }

    /**
     * Get a game from id
     * 
     * @param id 
     */
    public async getGame(id: ObjectId) {

        Validator.validate(this.collections, this.collections.games, id);

        /* eslint-disable  @typescript-eslint/no-non-null-assertion */

        const result = (await this.collections!.games!.findOne( { "_id": id }));

        if(!result) {
            return null;
        }
        
        return Game.generate(new ObjectId(result?._id), result?.username, result?.gamename, result?.creationDate);
    }

    /**
     * Post a new game
     * 
     * @param username 
     * @param gamename 
     */
    public async postGame(username: string, gamename: string) {

        Validator.validate(this.collections, this.collections.games, username, gamename);

        const game = Game.create(username, gamename);
        
        /* eslint-disable  @typescript-eslint/no-non-null-assertion */
        await this.collections!.games!.insertOne(game);

        return game._id; 
    }

    /**
     * Delete a game from id
     * 
     * @param id 
     */
    public async deleteGame(id: ObjectId) {

        Validator.validate(this.collections, this.collections.games, id);

        /* eslint-disable  @typescript-eslint/no-non-null-assertion */
        await this.collections!.games!.deleteOne({ "_id": id });

        return id;
    }

    /**
     * Patch a game identified by id
     * 
     * @param id
     * @param gamename 
     */
    public async patchGame(id: ObjectId, gamename: string) {

        Validator.validate(this.collections, this.collections.games, gamename);

        /* eslint-disable  @typescript-eslint/no-non-null-assertion */
        await this.collections!.games!.updateOne({ "_id" : id }, { $set: { "gamename": gamename } });

        return id;
    }

}

export { Database };