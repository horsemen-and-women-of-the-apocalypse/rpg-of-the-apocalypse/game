/**
 * @licence
 * Copyright 2022-2022 - RPG of the Apocalypse
 * Licensed under MIT or any later version
 * Refer to the LICENSE file included.
 */
import cors, { CorsOptions } from "cors";
import express, { NextFunction, Request, Response } from "express";
import { Server } from "http";
import responseTime from "response-time";
import LOGGER from "./logger";
import routes from "./routes";
import config from "./config/service";
import configAuth from "./config/auth";
import swaggerUi from "swagger-ui-express";
import swaggerConfig from "./config/swagger";
import { ApiError, ApiErrorResponse } from "@rpga/common";
import { HttpConfiguration, ApiConfiguration } from "./config/configuration";
import axios from "axios";

// Create server
const app = express();
const server = new Server(app);

// Set up CORS
const corsConfig: CorsOptions = { origin: true, credentials: true };
app.use(cors(corsConfig));

// Use JSON decoder for "application/json" body
app.use(express.json());

// Check token validity
app.use(function(req, resp, next) {
    
    const token = String(req.headers.token);
    
    axios.get(`${(configAuth.config as ApiConfiguration).host}:${(configAuth.config as ApiConfiguration).port}/auth/verify`, {
        headers: {
            "authorization": token
        },
        timeout: 2000
    }).then(() => {
        next();
    }).catch((error) => {
        if(error.response) {
            resp.status(error.response.status).json(new ApiErrorResponse(error.response.data.error));
        } else {
            resp.status(500).json(new ApiErrorResponse("INVALID_API_AUTH"));
        }   
    });
});

// Add response time
app.use(responseTime((req, res, time) => {
    // req.baseUrl exits, trust me :)
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    LOGGER.info(`[Express] ${req.method} ${(req as any).baseUrl} in ${time.toFixed(3)}ms => response code: ${res.statusCode}`);
}));

// Add routes
for (const route of routes) {
    app.use(route.base, route.router);
}

// Add Swagger UI
app.use("/docs", swaggerUi.serve, swaggerUi.setup(swaggerConfig));

// Start server
const PORT = (config.config as HttpConfiguration).port;
server.listen(PORT, async() => {
    LOGGER.info(`[Main] App listening on port: ${PORT} (mode: ${process.env.NODE_ENV})`);

    try {
        // Set up error handler
        app.use((error: Error, req: Request, res: Response, next: NextFunction) => {
            LOGGER.error(error);

            // Send response
            res.status(error instanceof ApiError ? (error as ApiError).code : 500).json(new ApiErrorResponse(error.message));
            next();
        });
    } catch (e) {
        LOGGER.error((e as Error).stack);
        process.exit(1);
    }
});

export default server;