/**
 * @licence
 * Copyright 2022-2022 - RPG of the Apocalypse
 * Licensed under MIT or any later version
 * Refer to the LICENSE file included.
 */

 type IniSection = { [property: string]: string };
 type Ini = { [section: string]: IniSection };

/**
 * Enum for config
 */
enum EConfig {
    SERVICE,
    AUTH,
    WORLD,
    DATABASE
}

/**
 * Parent class for configuration
 */
class IConfig {}

/**
 * Service configuration
 */
class Configuration {
    
    public config: IConfig;

    /**
     * Constructor
     *
     * @param conf Configuration as an object
     */
    constructor(conf: Ini, type: EConfig) {

        switch(type) {
            case EConfig.SERVICE: {

                const http = conf.http;
                
                this.check(http, type);
                
                this.config = new HttpConfiguration(http);
                break;
            }
            case EConfig.AUTH: {

                const auth = conf.auth;

                this.check(auth, type);

                this.config = new ApiConfiguration(auth);
                break;
            }
            case EConfig.WORLD: {

                const world = conf.world;

                this.check(world, type);

                this.config = new ApiConfiguration(world);
                break;
            }
            case EConfig.DATABASE: {

                const database = conf.database;

                this.check(database, type);

                this.config = new DatabaseConfiguration(database);
                break;
            }
            default: 
                throw new Error("Unknow configuration");
        }      
    }

    public check(conf: IniSection, type: EConfig) {
        if (conf === undefined || conf === null) {
            throw new Error(type.toString + " section is undefined in the configuration file");
        }
    }
}

/**
 * API Configurations
 */
class ApiConfiguration implements IConfig {

    /** Api host */
    public readonly host: string;

    /** Api port */
    public readonly port: number;

    constructor(conf: IniSection) {
        
        const host = conf.host;
        const port = Number.parseInt(conf.port);

        if(!host || host == "") {
            throw new Error(`External api host (value: ${host}) isn't valid in the configuration file`);
        }

        if (!Number.isInteger(port)) {
            throw new Error(`External api port (value: ${port}) isn't an integer in the configuration file`);
        }
        
        this.host = host;
        this.port = port;
    }
}

/**
 * Database Configurations
 */
class DatabaseConfiguration implements IConfig {

    /** Database name */
    public readonly name: string;

    /** Database url */
    public readonly url: string;

    /**
     * Constructor
     *
     * @param conf Configuration as an object
     */
    constructor(conf: IniSection) {
        this.name = conf.name;
        this.url = conf.url;
    }
}

/**
 * HTTP Server configuration
 */
class HttpConfiguration implements IConfig {
    
    /** Port number */
    public readonly port: number;

    /**
     * Constructor
     *
     * @param conf Configuration as an object
     */
    constructor(conf: IniSection) {
        const port = Number.parseInt(conf.port);
        if (!Number.isInteger(port)) {
            throw new Error(`http.number (value: ${port}) isn't an integer in the configuration file`);
        }
        this.port = port;
    }
}

export { Configuration, EConfig, ApiConfiguration, DatabaseConfiguration, HttpConfiguration };