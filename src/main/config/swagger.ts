/**
 * @licence
 * Copyright 2022-2022 - RPG of the Apocalypse
 * Licensed under MIT or any later version
 * Refer to the LICENSE file included.
 */
import { createApiResponseDefinition } from "@rpga/common";

const swaggerConfig = {
    "swagger": "2.0",
    "info": {
        "version": process.env.npm_package_version,
        "title": "RPG of the Apocalypse - Game service",
        "description": "Documentation describing the endpoints of the game service",
        "license": {
            "name": "MIT",
            "url": "https://opensource.org/licenses/MIT"
        }
    },
    "tags": [
        {
            "name": "Version",
            "description": "API for version"
        },
        {
            "name": "Games",
            "description": "API for games"
        }
    ],
    "basePath": "/",
    "schemes": [
        "http"
    ],
    "consumes": [
        "application/json"
    ],
    "produces": [
        "application/json"
    ],
    "securityDefinitions": {
        "TokenHeader": {
            "type": "apiKey",
            "in": "cookie",
            "name": "token"
        }
    },
    "paths": {
        "/version": {
            "get": {
                "tags": [ "Version" ],
                "summary": "Get the version of the API",
                "responses": {
                    "200": {
                        "description": "The version of the API",
                        "schema": {
                            "$ref": "#/definitions/VersionResponse"
                        }
                    }
                }
            }
        },
        "/api/v1/{username}/games": {
            "get": {
                "tags": [ "Games" ],
                "summary": "Get all games from user",
                "security": {
                    "TokenHeader": []
                },
                "parameters": [
                    {
                        "$ref": "#/components/schemas/Username"          
                    },
                    {
                        "in": "query",
                        "name": "search",
                        "description": "Search by gamename",
                        "required": false,
                        "schema": {
                            "type": "text",
                        }
                    },
                    {
                        "in": "query",
                        "name": "start",
                        "description": "Start limit to return games",
                        "required": false,
                        "schema": {
                            "type": "integer",
                            "minimum": 0
                        }
                    },
                    {
                        "in": "query",
                        "name": "end",
                        "description": "End limit to return games",
                        "required": false,
                        "schema": {
                            "type": "integer",
                            "minimum": 0,
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "List of games",
                        "schema": {
                            "$ref": "#/definitions/GameListGetResponse"
                        }
                    },
                    "400": {
                        "description": "INVALID_NUMBER : Start should be less than end and both should be integer",
                        "schema": {
                            "$ref": "#/definitions/ResponseError"
                        }
                    },
                    "500": {
                        "description": "INVALID_BDD_REQUEST : Error happened during bdd request",
                        "schema": {
                            "$ref": "#/definitions/ResponseError"
                        }
                    }
                }
            },
            "post": {
                "tags": [ "Games" ],
                "summary": "Post a new game for user",
                "security": {
                    "TokenHeader": []
                },
                "parameters": [
                    {
                        "$ref": "#/components/schemas/Username"          
                    },
                    {
                        "$ref": "#/components/schemas/Gamename"          
                    }
                ],
                "responses": {
                    "200": {
                        "description": "Status response",
                        "schema": {
                            "$ref": "#/definitions/BasicResponse"
                        }
                    },
                    "500": {
                        "description": "INVALID_BDD_REQUEST : Error happened during bdd request",
                        "schema": {
                            "$ref": "#/definitions/ResponseError"
                        }
                    },
                    "503": {
                        "description": "INVALID_API_WORLD : World api is unreachable",
                        "schema": {
                            "$ref": "#/definitions/ResponseError"
                        }
                    },
                }
            }
        },
        "/api/v1/{username}/games/{id}": {
            "get": {
                "tags": [ "Games" ],
                "summary": "Get specific game from user",
                "security": {
                    "TokenHeader": []
                },
                "parameters": [
                    {
                        "$ref": "#/components/schemas/Username"          
                    },
                    {
                        "$ref": "#/components/schemas/Id"          
                    }
                ],
                "responses": {
                    "200": {
                        "description": "Game specified by id",
                        "schema": {
                            "$ref": "#/definitions/GameGetResponse"
                        }
                    },
                    "400": {
                        "description": "INVALID_ID : The id does not match with mongo ObjectId",
                        "schema": {
                            "$ref": "#/definitions/ResponseError"
                        }
                    },
                    "500": {
                        "description": "INVALID_BDD_REQUEST : Error happened during bdd request",
                        "schema": {
                            "$ref": "#/definitions/ResponseError"
                        }
                    }
                }
            },
            "delete": {
                "tags": [ "Games" ],
                "summary": "Delete specific game from user",
                "security": {
                    "TokenHeader": []
                },
                "parameters": [
                    {
                        "$ref": "#/components/schemas/Username"          
                    },
                    {
                        "$ref": "#/components/schemas/Id"          
                    }
                ],
                "responses": {
                    "200": {
                        "description": "Status response",
                        "schema": {
                            "$ref": "#/definitions/BasicResponse"
                        }
                    },
                    "400": {
                        "description": "INVALID_ID : The id does not match with mongo ObjectId",
                        "schema": {
                            "$ref": "#/definitions/ResponseError"
                        }
                    },
                    "500": {
                        "description": "INVALID_BDD_REQUEST : Error happened during bdd request",
                        "schema": {
                            "$ref": "#/definitions/ResponseError"
                        }
                    },
                    "503": {
                        "description": "INVALID_API_WORLD : World api is unreachable",
                        "schema": {
                            "$ref": "#/definitions/ResponseError"
                        }
                    }
                }
            },
            "patch": {
                "tags": [ "Games" ],
                "summary": "Patch specific game from user",
                "security": {
                    "TokenHeader": []
                },
                "parameters": [
                    {
                        "$ref": "#/components/schemas/Username"          
                    },
                    {
                        "$ref": "#/components/schemas/Id"          
                    },
                    {
                        "$ref": "#/components/schemas/Gamename"          
                    }
                ],
                "responses": {
                    "200": {
                        "description": "Status response",
                        "schema": {
                            "$ref": "#/definitions/BasicResponse"
                        }
                    },
                    "400": {
                        "description": "INVALID_ID : The id does not match with mongo ObjectId",
                        "schema": {
                            "$ref": "#/definitions/ResponseError"
                        }
                    },
                    "500": {
                        "description": "INVALID_BDD_REQUEST : Error happened during bdd request",
                        "schema": {
                            "$ref": "#/definitions/ResponseError"
                        }
                    },
                }
            }
        }
    },
    "definitions": {
        "VersionResponse": createApiResponseDefinition("#/definitions/VersionResponseData"),
        "VersionResponseData": {
            "type": "string",
            "description": "{major}.{minor}.{patch} or {major}.{minor}.{patch}-{label}"
        },
        "GameListGetResponse": createApiResponseDefinition("#/definitions/GameListGetResponseData"),
        "GameListGetResponseData": {
            "type": "array",
            "items": {
                "$ref": "#/components/schemas/Game"
            }
        },
        "BasicResponse": createApiResponseDefinition("#/definitions/BasicResponse"),
        "BasicResponseData": {
            "type": "string"
        },
        "GameGetResponse": createApiResponseDefinition("#/definitions/GameGetResponseData"),
        "GameGetResponseData": {
            "type": "object",
            "$ref": "#/components/schemas/Game" 
        },
        "ResponseError": createApiResponseDefinition("#/definitions/ResponseErrorData"),
        "ResponseErrorData": {
            "type": "object",
            "$ref": null
        },
    },
    "components": {
        "schemas": {
            "Username": {
                "in": "path",
                "name": "username",
                "required": "true",
                "type": "string",
                "description": "User name"
            },
            "Gamename": {
                "in": "body",
                "name": "gamename",
                "required": "true",
                "type": "object",
                "description": "Game name",
                "example": {
                    "gamename": "string"
                }
            },
            "GameParamsArray": {
                "in": "body",
                "name": "gamename",
                "required": "false",
                "type": "object",
                "description": "Game name",
                "example": {
                    "gamename": "string"
                }
            },
            "Id": {
                "in": "path",
                "name": "_id",
                "required": "true",
                "type": "ObjectId",
                "description": "Game id"
            },
            "Game": {
                "type": "object",
                "example": {
                    "_id": "ObjectId",
                    "username": "string",
                    "gamename": "string",
                    "creationDate": "Date"
                }  
            }
        }
    }
};

export default swaggerConfig;
