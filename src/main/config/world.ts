/**
 * @licence
 * Copyright 2022-2022 - RPG of the Apocalypse
 * Licensed under MIT or any later version
 * Refer to the LICENSE file included.
 */
import fs from "fs";
import ini from "ini";
import { Configuration, EConfig } from "./configuration";
 
// Load configuration
const configPath = "./config/app." + process.env.NODE_ENV + ".ini";
const config = new Configuration(ini.parse(fs.readFileSync(configPath, { encoding: "utf-8" })), EConfig.WORLD);

export default config;